let inputNome = null;
let inputSenha = null;
let inputCSenha = null;
let validarNome = null;
let validarSenha= null;
let validarCSenha = null;
let status= null;

console.log("Carregou");

function executar() {
    mapearDOM();
    inputNome.addEventListener('onblur', validarInputNome);
}

executar();


// window.addEventListener('load', () => {
//     // mapearDOM();
//     let inputNome = document.querySelector("#nome");
//     let inputSenha = document.querySelector("#senha");
//     let inputCSenha = document.querySelector("#csenha");
//     let validarNome = document.querySelector(".validarNome");
//     let validarSenha = document.querySelector(".validarSenha");
//     let validarCSenha = document.querySelector(".validarCSenha");

//     let status = document.querySelector("p.status");
//     let alterarBtn = document.querySelector("#alterarBtn");

//     inputNome.addEventListener('onblur', validarInputNome);
//     inputSenha.addEventListener('onblur', validarInputSenha);
//     inputCSenha.addEventListener('onblur', validarInputConfirmarSenha);
//     // alterarBtn.addEventListener('onclick', gerenciarStatus);
//     alterarBtn.addEventListener('onclick', testar);
    
// });

function testar() {
    console.log("teste");
}

function mapearDOM() {
    inputNome = document.querySelector("#nome");
    inputSenha = document.querySelector("#senha");
    inputCSenha = document.querySelector("#csenha");
    validarNome = document.querySelector(".validarNome");
    validarSenha = document.querySelector(".validarSenha");
    validarCSenha = document.querySelector(".validarCSenha");

    status = document.querySelector("p.status");
    alterarBtn = document.getElementById("alterarBtn");
    
}

// inputNome.addEventListener('onblur', validarInputNome);
// inputSenha.addEventListener('onblur', validarInputSenha);
// inputCSenha.addEventListener('onblur', validarInputConfirmarSenha);


function validarInputNome() {
    if(!(inputNome.value != "")) {
        validarNome.innerText = "Campo obrigatório, por favor preencha";
    }
}

function validarInputSenha() {
    if(inputSenha.value == "") {
        validarSenha.innerText = "Campo obrigatório, por favor preencha";
    }
}

function validarInputConfirmarSenha() {
    if(inputCSenha.value != inputSenha.value) {
        validarSenha.innerText = "Os campos devem ser iguais";
    }
}

function gerenciarStatus() {
    if(status.innerText == 'Inativo') {
        status.classList.add('ativo');
        status.innerText = 'Ativo';
    }

    if(status.innerText == 'Ativo') {
        status.classList.remove('ativo');
        status.innerText = 'Inativo';
    }
}