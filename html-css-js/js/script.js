/* Setando variáveis globais */
let inputNome = null;
let inputSenha = null;
let inputCSenha = null;

let validarNome = null;
let validarSenha = null;
let validarCSenha = null;

let status = null;
let alterarBtn = null;

/*  Ao carregar a página (window), 
    invocará uma função anônima usando arrow function

    addEventListener => adiciona um escutador de evento a algum elemento
                        quando o evento ocorrer, a função dispara
*/
window.addEventListener('load', () => {
    /* Invoca a função de mapeamento */
    mapearDOM();

    inputNome.addEventListener('blur', validarInputNome);
    inputSenha.addEventListener('blur', validarInputSenha);
    inputCSenha.addEventListener('change', validarConfirmarSenha);

    alterarBtn.addEventListener('click', gerenciarStatus);
});


/* Encapsula o mapeamento do DOM */
function mapearDOM() {
    inputNome = document.querySelector(".inputNome");
    inputSenha = document.querySelector(".inputSenha");
    inputCSenha = document.querySelector(".inputCSenha");

    validarNome = document.getElementById("validarNome");
    validarSenha = document.getElementById("validarSenha");
    validarCSenha = document.getElementById("validarCSenha");

    status = document.getElementById("status");
    alterarBtn = document.getElementById("alterarBtn");
}

function validarInputNome() {
    if(inputNome.value == "") {
        validarNome.innerText = "Campo obrigatório, por favor preencha";
    } else {
        validarNome.innerText = "";
    }
}

function validarInputSenha() {
    if(inputSenha.value == "") {
        validarSenha.innerText = "Campo obrigatório, por favor preencha";
    } else {
        validarSenha.innerText = "";
    }
}

function validarConfirmarSenha() {
    if(inputCSenha.value != inputSenha.value) {
        validarCSenha.innerText = "A confirmação não bate com a senha";
    } else {
        validarCSenha.innerText = "";
    }
}

function gerenciarStatus() {
    if(status.innerText == 'Inativo') {
        status.classList.add('ativo');
        status.innerText = 'Ativo';
    } else if (status.innerText == 'Ativo') {
        status.classList.remove('ativo');
        status.innerText = 'Inativo';
    }
}
